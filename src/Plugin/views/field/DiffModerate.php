<?php

namespace Drupal\diff_moderate\Plugin\views\field;

use Drupal\Core\Url;
use Drupal\views\Plugin\views\field\LinkBase;
use Drupal\views\ResultRow;

/**
 * Provides View field diff from plugin.
 *
 * @ViewsField("diff__moderate")
 */
class DiffModerate extends LinkBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['label']['default'] = t('Review changes and moderate');
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $row) {
    $entity = $row->_entity;
    $nid = (integer) $entity->id();
    $rev_key = $entity->getEntityType()->getKey('revision');
    // Current published revision.
    // We have the current revision so to get the published one we load the node fresh.
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
    $published_vid = (integer) $node->getRevisionId();
    // Latest draft revision.
    $latest_vid = (integer) \Drupal::entityTypeManager()->getStorage('node')->getLatestRevisionId($entity->id());
    $row->_revision_id_published = $published_vid;
    $row->_revision_id_draft = $latest_vid;
    return parent::render($row);
  }

  protected function getUrlInfo(ResultRow $row): Url {
    $nid = $row->_entity->id();
    $published_vid = $row->_revision_id_published;
    $latest_vid = $row->_revision_id_draft;
    // Note that the identity === only works because we caste to integer above.
    if (!$published_vid || $published_vid === $latest_vid) {
      // Attempting to go to a link comparing identical revisions goes very
      // badly (infinite loop).  Trying to compare to a nonexistent (null)
      // revision ID goes pretty badly too (fatal error), though that shouldn't
      // happen.  But for any such case we show the regular node view at the
      // latest revision which should be configured to show the moderation form.
      return Url::fromRoute('entity.node.revision', [
        'node' => $nid,
        'node_revision' => $latest_vid,
      ]);
    }
    return Url::fromRoute('diff_moderate.revisions_diff', [
      'node' => $nid,
      'left_revision' => $published_vid,
      'right_revision' => $latest_vid,
      'filter' => 'visual_inline',
    ]);
  }

}
