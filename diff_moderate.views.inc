<?php

/**
 * @file
 * Provide views data for diff_moderate.module.
 */

/**
 * Implements hook_views_data().
 */
function diff_moderate_views_data() {
  $data = [];

  /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type */
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type) {
    // We are only doing nodes for now.
    if ($entity_type->id() !== 'node') {
      continue;
    }
    // Add the revision diff moderate field to every revisionable entity type.
    if ($entity_type->isRevisionable()) {
      $revision_base_table = $entity_type->getRevisionDataTable() ?: $entity_type->getRevisionTable();

      $data[$revision_base_table]['diff_moderate'] = [
        'title' => t('Diff moderate'),
        'help' => 'Link to diff of revisions where one can use the moderation control form.',
        'field' => [
          'id' => 'diff__moderate',
        ],
      ];
    }
  }

  return $data;
}
